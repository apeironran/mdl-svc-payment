package com.mydoctorlink.payment.domain;

import com.stripe.exception.StripeException;
import com.stripe.model.PaymentIntent;
import com.stripe.param.PaymentIntentCreateParams;
import org.springframework.stereotype.Component;

@Component
public class StripePaymentIntent {

    public PaymentIntent create(PaymentIntentCreateParams paymentDetail) throws StripeException {
        return PaymentIntent.create(paymentDetail);
    }

    public PaymentIntent searchByPaymentIntentId(String paymentIntentId) throws StripeException {
        return PaymentIntent.retrieve(paymentIntentId);
    }

}
