package com.mydoctorlink.payment.domain;

import com.stripe.exception.StripeException;
import com.stripe.model.Customer;
import com.stripe.model.CustomerCollection;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class StripeCustomer {

    public Customer create(Map<String, Object> customerDetail) throws StripeException {
        return Customer.create(customerDetail);
    }

    public Customer searchByEmail(String email) throws StripeException {
        Map<String, Object> searchByParams = new HashMap<>();
        searchByParams.put("email", email);
        CustomerCollection customerCollection = Customer.list(searchByParams);
        List<Customer> customerList = customerCollection.getData();

        if(customerList != null && !customerList.isEmpty()){
            return customerList.get(0);
        }
        return null;
    }
}
