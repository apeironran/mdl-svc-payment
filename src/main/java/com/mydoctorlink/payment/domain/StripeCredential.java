package com.mydoctorlink.payment.domain;

import com.mydoctorlink.payment.common.JsonUtility;
import org.springframework.stereotype.Component;

@Component
public class StripeCredential extends JsonUtility<StripeCredential> {

    public String key;

    public StripeCredential(){setDeserializationClass(StripeCredential.class);}
}
