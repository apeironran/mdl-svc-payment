package com.mydoctorlink.payment.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class JsonUtility<T> {
    final static protected ObjectMapper objectMapper = new ObjectMapper();
    @JsonIgnore
    private Class<T> deserializationClass;

    public void setDeserializationClass(Class<T> targetClass){
        deserializationClass = targetClass;
    }

    public T deserialization(String jsonString) throws JsonProcessingException {
        return objectMapper
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, true)
                .configure(DeserializationFeature.FAIL_ON_MISSING_CREATOR_PROPERTIES, true)
                .readValue(jsonString, deserializationClass);
    }

    public String serialization(T targetObject) throws JsonProcessingException {
        return objectMapper
                .configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
                .writeValueAsString(targetObject);
    }
}
