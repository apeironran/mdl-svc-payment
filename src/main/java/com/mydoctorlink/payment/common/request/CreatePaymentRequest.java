package com.mydoctorlink.payment.common.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.mydoctorlink.payment.common.JsonUtility;
import org.springframework.stereotype.Component;

@Component
public class CreatePaymentRequest extends JsonUtility<CreatePaymentRequest> {

    public Long amount;
    public String currency;
    public String customerId;

    public CreatePaymentRequest(){setDeserializationClass(CreatePaymentRequest.class);}

    @JsonCreator
    public CreatePaymentRequest(@JsonProperty(value = "amount", required = true) Long _amount, @JsonProperty(value = "currency", required = true) String _currency, @JsonProperty(value="customerId", required = true) String _customerId){
        this.amount = _amount;
        this.currency = _currency;
        this.customerId = _customerId;
    }

    public void selfConstruct(String params) throws JsonProcessingException {
        CreatePaymentRequest instance = this.deserialization(params);
        this.amount = instance.amount;
        this.currency = instance.currency;
        this.customerId = instance.customerId;
    }
}
