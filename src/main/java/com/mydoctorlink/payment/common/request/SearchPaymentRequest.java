package com.mydoctorlink.payment.common.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.mydoctorlink.payment.common.JsonUtility;
import org.springframework.stereotype.Component;

@Component
public class SearchPaymentRequest extends JsonUtility<SearchPaymentRequest> {

    public String paymentIntentId;

    public SearchPaymentRequest(){setDeserializationClass(SearchPaymentRequest.class);}

    @JsonCreator
    public SearchPaymentRequest(@JsonProperty(value = "paymentIntentId", required = true) String _paymentIntentId){
        this.paymentIntentId = _paymentIntentId;
    }

    public void selfConstruct(String params) throws JsonProcessingException {
        SearchPaymentRequest instance = this.deserialization(params);
        this.paymentIntentId = instance.paymentIntentId;
    }
}
