package com.mydoctorlink.payment.common.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.mydoctorlink.payment.common.JsonUtility;
import org.springframework.stereotype.Component;

@Component
public class HandlerRequest  extends JsonUtility<HandlerRequest> {
    public String mdlRequestId;
    public String action;
    public JsonNode payload;

    public HandlerRequest(){
        setDeserializationClass(HandlerRequest.class);
    }

    @JsonCreator
    public HandlerRequest(@JsonProperty(value = "mdlRequestId", required = true) String _mdlRequestId, @JsonProperty(value = "action", required = true) String _action, @JsonProperty(value = "payload", required = true) JsonNode _payload){
        this.mdlRequestId = _mdlRequestId;
        this.action = _action;
        this.payload = _payload;
    }
}
