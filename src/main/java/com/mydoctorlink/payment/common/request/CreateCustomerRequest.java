package com.mydoctorlink.payment.common.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.mydoctorlink.payment.common.JsonUtility;
import org.springframework.stereotype.Component;

@Component
public class CreateCustomerRequest extends JsonUtility<CreateCustomerRequest> {

    public String name;
    public String email;

    public CreateCustomerRequest(){setDeserializationClass(CreateCustomerRequest.class);}

    @JsonCreator
    public CreateCustomerRequest(@JsonProperty(value = "name") String _name, @JsonProperty(value = "email", required = true) String _email){
        this.name = _name;
        this.email = _email;
    }

    public void selfConstruct(String params) throws JsonProcessingException {
        CreateCustomerRequest instance = this.deserialization(params);
        this.name = instance.name;
        this.email = instance.email;
    }

}
