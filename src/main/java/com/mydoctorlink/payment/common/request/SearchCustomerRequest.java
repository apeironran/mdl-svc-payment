package com.mydoctorlink.payment.common.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.mydoctorlink.payment.common.JsonUtility;
import org.springframework.stereotype.Component;

@Component
public class SearchCustomerRequest extends JsonUtility<SearchCustomerRequest> {

    public String email;

    public SearchCustomerRequest(){setDeserializationClass(SearchCustomerRequest.class);}

    @JsonCreator
    public SearchCustomerRequest(@JsonProperty(value = "email", required = true) String _email){
        this.email = _email;
    }

    public void selfConstruct(String params) throws JsonProcessingException {
        SearchCustomerRequest instance = this.deserialization(params);
        this.email = instance.email;
    }

}
