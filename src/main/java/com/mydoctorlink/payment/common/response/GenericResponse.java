package com.mydoctorlink.payment.common.response;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.mydoctorlink.payment.common.JsonUtility;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class GenericResponse extends JsonUtility<GenericResponse> {
    public ResponseStatus status;
    public Object data;

    public GenericResponse(){
        setDeserializationClass(GenericResponse.class);
    }

    public String serialization() throws JsonProcessingException {
        return super.serialization(this);
    }

    @JsonIgnore
    public String getErrorJson(){
        return "{\"status\":\"" + this.status + "\", \"error\":\""+this.data+"\"}";
    }
}
