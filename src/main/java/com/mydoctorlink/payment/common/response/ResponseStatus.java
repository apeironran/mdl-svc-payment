package com.mydoctorlink.payment.common.response;

public enum ResponseStatus {
    OK,
    ERROR_OCCURRED
}
