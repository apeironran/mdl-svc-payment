package com.mydoctorlink.payment;

import com.mydoctorlink.payment.common.request.HandlerRequest;
import com.mydoctorlink.payment.common.response.GenericResponse;
import com.mydoctorlink.payment.common.response.ResponseStatus;
import com.mydoctorlink.payment.stripe.StripeServiceFactory;
import com.mydoctorlink.payment.stripe.StripeServiceOptions;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.function.Function;

@SpringBootApplication
public class PaymentServiceHandler implements Function<String, String> {

    private final Logger logger = LogManager.getLogger(PaymentServiceHandler.class);
    private StripeServiceFactory stripePaymentUtilityFactory;
    private HandlerRequest handlerRequest;


    public static void main(String[] args){
        SpringApplication.run(PaymentServiceHandler.class, args);
    }

    @Autowired
    public void setStripePaymentUtilityFactory(StripeServiceFactory factory){
        stripePaymentUtilityFactory = factory;
    }

    @Autowired
    public void setHandlerRequest(HandlerRequest request){
        handlerRequest = request;
    }

    @Override
    public String apply(String request) {
        try {
            logger.info("Request Id: "+handlerRequest.deserialization(request).mdlRequestId);
            return stripePaymentUtilityFactory
                    .getService(StripeServiceOptions.valueOf(handlerRequest.deserialization(request).action))
                    .process(handlerRequest.deserialization(request).payload.toString());
        }catch (Exception e){
            logger.error(e);
            GenericResponse response = new GenericResponse();
            response.status = ResponseStatus.ERROR_OCCURRED;
            response.data = e;
            return response.getErrorJson();
        }
    }
}
