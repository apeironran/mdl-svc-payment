package com.mydoctorlink.payment.aws;

public interface ISecretManagerHelper {
    String getSecret(String secretId);
}
