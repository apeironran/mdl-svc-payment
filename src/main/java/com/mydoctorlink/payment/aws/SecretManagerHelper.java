package com.mydoctorlink.payment.aws;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.secretsmanager.SecretsManagerClient;
import software.amazon.awssdk.services.secretsmanager.SecretsManagerClientBuilder;
import software.amazon.awssdk.services.secretsmanager.model.GetSecretValueRequest;
import software.amazon.awssdk.services.secretsmanager.model.GetSecretValueResponse;

@Component
public class SecretManagerHelper implements ISecretManagerHelper {
    final private static SecretsManagerClientBuilder clientBuilder = SecretsManagerClient.builder();
    final private static GetSecretValueRequest.Builder requestBuilder = GetSecretValueRequest.builder();
    final private static Logger logger = LogManager.getLogger(SecretManagerHelper.class);

    public String getSecret(String secretId){
        logger.info("Fetching api key from "+secretId);
        try {
            GetSecretValueResponse secretValue = clientBuilder
                    .region(Region.AP_SOUTHEAST_2)
                    .build()
                    .getSecretValue(requestBuilder.secretId(secretId).build());
            return secretValue.secretString();
        }catch (Exception e){
            logger.error(e);
            return null;
        }
    }
}
