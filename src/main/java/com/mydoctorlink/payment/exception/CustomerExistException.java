package com.mydoctorlink.payment.exception;

public class CustomerExistException extends Exception{

    public CustomerExistException(String message){
        super(message);
    }
}
