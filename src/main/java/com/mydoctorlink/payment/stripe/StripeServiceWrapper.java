package com.mydoctorlink.payment.stripe;

import com.mydoctorlink.payment.common.response.GenericResponse;
import com.mydoctorlink.payment.common.response.ResponseStatus;
import com.mydoctorlink.payment.domain.StripeCustomer;
import com.mydoctorlink.payment.domain.StripePaymentIntent;
import com.stripe.Stripe;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class StripeServiceWrapper {
    final protected static Logger logger = LogManager.getLogger(StripeServiceWrapper.class);
    final protected GenericResponse response = new GenericResponse();
    protected StripeCustomer stripeCustomerApi;
    protected StripePaymentIntent stripePaymentApi;
    abstract protected void handleService(String serviceRequests) throws Exception;

    @Autowired
    private void setStripeCustomer(StripeCustomer stripeCustomer){
        stripeCustomerApi = stripeCustomer;
    }

    @Autowired
    private void setStripePaymentIntent(StripePaymentIntent stripePayment){
        stripePaymentApi = stripePayment;
    }

    @Autowired
    public void setStripeCredentialStrategy(IStripeCredentialStrategy strategy){
        Stripe.apiKey = strategy.generate();
    }

    public String process(String serviceRequest) {
        try {
            response.status = ResponseStatus.OK;
            this.handleService(serviceRequest);
            return response.serialization();
        }catch (Exception e){
            logger.error(e);
            response.status = ResponseStatus.ERROR_OCCURRED;
            response.data = e.getMessage();
            return response.getErrorJson();
        }
    }
}
