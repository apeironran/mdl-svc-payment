package com.mydoctorlink.payment.stripe.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mydoctorlink.payment.common.request.CreateCustomerRequest;
import com.mydoctorlink.payment.common.response.ResponseStatus;
import com.mydoctorlink.payment.exception.CustomerExistException;
import com.mydoctorlink.payment.stripe.StripeServiceWrapper;
import com.stripe.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class CreateCustomerService extends StripeServiceWrapper {

    private final Map<String, Object> customerDetail = new HashMap<>();
    private CreateCustomerRequest createCustomerRequest;

    @Autowired
    private void setCreateCustomerRequest(CreateCustomerRequest request){
        createCustomerRequest = request;
    }

    @Override
    protected void handleService(String params) throws Exception {
        createCustomerRequest.selfConstruct(params);
        customerDetail.put("email",createCustomerRequest.email);
        customerDetail.put("name",createCustomerRequest.name);

        Customer existingCustomer = stripeCustomerApi.searchByEmail(createCustomerRequest.email);

        if(existingCustomer != null){
            throw new CustomerExistException("Email has been registered");
        }else{
            existingCustomer = stripeCustomerApi.create(customerDetail);
        }
        response.status = ResponseStatus.OK;
        response.data = (new ObjectMapper()).readTree(existingCustomer.toJson());
    }
}
