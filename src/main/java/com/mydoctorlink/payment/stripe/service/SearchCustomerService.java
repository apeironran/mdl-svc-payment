package com.mydoctorlink.payment.stripe.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mydoctorlink.payment.common.request.SearchCustomerRequest;
import com.mydoctorlink.payment.stripe.StripeServiceWrapper;
import com.stripe.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SearchCustomerService extends StripeServiceWrapper {

    SearchCustomerRequest searchCustomerRequest;

    @Autowired
    private void setSearchCustomerRequest(SearchCustomerRequest request){
        searchCustomerRequest = request;
    }

    @Override
    protected void handleService(String params) throws Exception {
        searchCustomerRequest.selfConstruct(params);
        Customer existingCustomer = stripeCustomerApi.searchByEmail(searchCustomerRequest.email);
        if(existingCustomer == null){
            existingCustomer = new Customer();
        }
        response.data = (new ObjectMapper()).readTree(existingCustomer.toJson());
    }

}