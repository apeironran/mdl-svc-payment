package com.mydoctorlink.payment.stripe.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mydoctorlink.payment.common.request.SearchPaymentRequest;
import com.mydoctorlink.payment.stripe.StripeServiceWrapper;
import com.stripe.model.PaymentIntent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SearchPaymentService extends StripeServiceWrapper {

    private SearchPaymentRequest searchPaymentRequest;

    @Autowired
    private void setSearchPaymentRequest(SearchPaymentRequest request){
        searchPaymentRequest = request;
    }

    @Override
    protected void handleService(String params) throws Exception {
        searchPaymentRequest.selfConstruct(params);
        PaymentIntent existingPaymentIntent = stripePaymentApi.searchByPaymentIntentId(searchPaymentRequest.paymentIntentId);
        if(existingPaymentIntent == null){
            existingPaymentIntent = new PaymentIntent();
        }
        response.data = (new ObjectMapper()).readTree(existingPaymentIntent.toJson());
    }
}
