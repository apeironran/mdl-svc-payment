package com.mydoctorlink.payment.stripe.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mydoctorlink.payment.common.request.CreatePaymentRequest;
import com.mydoctorlink.payment.stripe.StripeServiceWrapper;
import com.stripe.model.PaymentIntent;
import com.stripe.param.PaymentIntentCreateParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CreatePaymentService extends StripeServiceWrapper {

    private CreatePaymentRequest paymentRequest;

    @Autowired
    private void setCreatePaymentRequest(CreatePaymentRequest request){
        paymentRequest = request;
    }

    @Override
    protected void handleService(String params) throws Exception {
        paymentRequest.selfConstruct(params);
        PaymentIntent newPaymentIntent = stripePaymentApi.create(
                PaymentIntentCreateParams.builder()
                        .setAmount(paymentRequest.amount)
                        .setCurrency(paymentRequest.currency)
                        .setCustomer(paymentRequest.customerId)
                        .build());
        response.data = (new ObjectMapper()).readTree(newPaymentIntent.toJson());
    }
}
