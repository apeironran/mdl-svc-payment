package com.mydoctorlink.payment.stripe;

public enum StripeServiceOptions {
    CREATE_CUSTOMER,
    SEARCH_CUSTOMER,
    CREATE_PAYMENT_INTENT,
    SEARCH_PAYMENT_INTENT
}
