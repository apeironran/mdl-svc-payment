package com.mydoctorlink.payment.stripe;

import com.mydoctorlink.payment.aws.ISecretManagerHelper;
import com.mydoctorlink.payment.domain.StripeCredential;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class StripeTokenStrategy implements IStripeCredentialStrategy {

    final private static Logger logger = LogManager.getLogger(StripeTokenStrategy.class);

    @Value("${stripe.api.key.id}")
    private String apiKeyId;
    @Value("${aws.stage}")
    private  String stage;
    private ISecretManagerHelper secretManagerHelper;

    @Autowired
    public void setSecretManagerHelper(ISecretManagerHelper utility){
        secretManagerHelper = utility;
    }

    @Override
    public String generate() {
        try {
            String response = secretManagerHelper.getSecret(apiKeyId+"-"+stage);
            StripeCredential stripeCredential = (new StripeCredential()).deserialization(response);
            return stripeCredential.key;
        }catch (Exception e){
            logger.error(e);
            return null;
        }
    }
}
