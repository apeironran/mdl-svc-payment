package com.mydoctorlink.payment.stripe;

public interface IStripeCredentialStrategy {
    String generate();
}
