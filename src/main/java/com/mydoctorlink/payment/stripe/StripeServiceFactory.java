package com.mydoctorlink.payment.stripe;

import com.mydoctorlink.payment.stripe.service.CreateCustomerService;
import com.mydoctorlink.payment.stripe.service.CreatePaymentService;
import com.mydoctorlink.payment.stripe.service.SearchCustomerService;
import com.mydoctorlink.payment.stripe.service.SearchPaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Hashtable;
import java.util.Map;

@Component
public class StripeServiceFactory {
    private Map<StripeServiceOptions, StripeServiceWrapper> mapOfStripeActions = new Hashtable<>();

    @Autowired
    private void setCreateCustomerService(CreateCustomerService service){
        setService(StripeServiceOptions.CREATE_CUSTOMER, service);
    }

    @Autowired
    private void serSearchCustomerService(SearchCustomerService service){
        setService(StripeServiceOptions.SEARCH_CUSTOMER, service);
    }

    @Autowired
    private void setCreatePaymentService(CreatePaymentService service){
        setService(StripeServiceOptions.CREATE_PAYMENT_INTENT, service);
    }

    @Autowired
    private void setSearchPaymentService(SearchPaymentService service){
        setService(StripeServiceOptions.SEARCH_PAYMENT_INTENT, service);
    }

    public StripeServiceFactory setService(StripeServiceOptions option, StripeServiceWrapper action){
        mapOfStripeActions.put(option, action);
        return this;
    }

    public StripeServiceWrapper getService(StripeServiceOptions action){
        return mapOfStripeActions.get(action);
    }
}
