import com.fasterxml.jackson.core.JsonProcessingException;
import com.mydoctorlink.payment.common.JsonUtility;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = JsonUtility.class)
public class JsonUtilityTest {

    @Test
    void shouldSerializeObject() throws JsonProcessingException {
        JsonUtility<TestClass> jsonUtility = new JsonUtility<>();
        jsonUtility.setDeserializationClass(TestClass.class);
        assertEquals("{\"field1\":\"field1\",\"field2\":\"field2\"}", jsonUtility.serialization(new TestClass()));
    }

    @Test
    void shouldDeserializeObject() throws JsonProcessingException {
        JsonUtility<TestClass> jsonUtility = new JsonUtility<>();
        jsonUtility.setDeserializationClass(TestClass.class);
        TestClass testResult = jsonUtility.deserialization("{\"field1\":\"a\",\"field2\":\"b\"}");
        assertEquals("a", testResult.field1);
        assertEquals("b", testResult.field2);
    }

    @Test
    void shouldThrowErrorWhenFieldsMismatch() {
        assertThrows(JsonProcessingException.class, ()->{
            JsonUtility<TestClass> jsonUtility = new JsonUtility<>();
            jsonUtility.setDeserializationClass(TestClass.class);
            jsonUtility.deserialization("{\"field3\":\"a\",\"field4\":\"b\"}");
        });
    }

    private static class TestClass{
        public String field1 = "field1";
        public String field2 = "field2";
    }
}
