package com.mydoctorlink.payment;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mydoctorlink.payment.common.request.HandlerRequest;
import com.mydoctorlink.payment.stripe.StripeServiceFactory;
import com.mydoctorlink.payment.stripe.StripeServiceOptions;
import com.mydoctorlink.payment.stripe.StripeServiceWrapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PaymentServiceHandlerTest {

    @Mock
    StripeServiceFactory stripeServiceFactory;

    @Mock
    StripeServiceWrapper stripeServiceWrapper;

    @InjectMocks
    PaymentServiceHandler testClass;

    @Test
    void shouldSuccessfulProcessTheRequest() throws JsonProcessingException {
        String request = "{\"mdlRequestId\": \"123\",\"action\": \"CREATE_CUSTOMER\",\"payload\": {}}";
        when(stripeServiceWrapper.process(any())).thenReturn("{\"status\": \"OK\"}");
        when(stripeServiceFactory.getService(any(StripeServiceOptions.class))).thenReturn(stripeServiceWrapper);

        testClass.setHandlerRequest(new HandlerRequest());
        assertEquals("{\"status\": \"OK\"}",testClass.apply(request));
    }

    @Test
    void shouldThrowErrorWhenMissingField(){
        String request = "{\"mdlRequestId\": \"\",\"action\": \"CREATE_CUSTOMER\"}";
        testClass.setHandlerRequest(new HandlerRequest());
        assertThat(testClass.apply(request)).contains("MismatchedInputException");
    }

    @Test
    void shouldThrowErrorWithMismatchedField(){
        String request = "{\"mdlRequestId\": \"\",\"action\": \"CREATE_CUSTOMER\", \"payloads\": {}}";
        testClass.setHandlerRequest(new HandlerRequest());
        assertThat(testClass.apply(request)).contains("MismatchedInputException");
    }


}