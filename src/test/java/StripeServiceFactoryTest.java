import com.mydoctorlink.payment.stripe.StripeServiceFactory;
import com.mydoctorlink.payment.stripe.StripeServiceOptions;
import com.mydoctorlink.payment.stripe.service.CreateCustomerService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class StripeServiceFactoryTest {

    @Mock
    CreateCustomerService createCustomerService;

    @InjectMocks
    StripeServiceFactory testClass;

    @Test
    void shouldSetupFactory(){
        testClass.setService(StripeServiceOptions.CREATE_CUSTOMER, createCustomerService);
        assertNotNull(testClass.getService(StripeServiceOptions.CREATE_CUSTOMER));
    }
}