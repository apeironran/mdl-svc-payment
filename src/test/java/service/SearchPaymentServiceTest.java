package service;

import com.mydoctorlink.payment.common.request.SearchPaymentRequest;
import com.mydoctorlink.payment.domain.StripePaymentIntent;
import com.mydoctorlink.payment.stripe.service.SearchPaymentService;
import com.stripe.model.PaymentIntent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.regex.Pattern;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class SearchPaymentServiceTest {

    @Mock
    StripePaymentIntent paymentIntent;

    @Mock
    SearchPaymentRequest paymentRequest;

    @InjectMocks
    SearchPaymentService testClass;

    @BeforeEach
    void mockSetup() throws Exception{
        doAnswer(invocation->{
            ((SearchPaymentRequest)invocation.getMock()).paymentIntentId = "pi_dfsvsei94jcc";
            return null;
        }).when(paymentRequest).selfConstruct("");
    }

    @Test
    void shouldFindThePayment() throws Exception{
        when(paymentIntent.searchByPaymentIntentId(anyString())).thenReturn(new PaymentIntent());
        assertThat(testClass.process("")).matches(Pattern.compile("\\{\"status\":\"OK\"(.+)"));
        verify(paymentRequest).selfConstruct("");
        verify(paymentIntent).searchByPaymentIntentId(anyString());
    }
}