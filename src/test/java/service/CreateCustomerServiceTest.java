package service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mydoctorlink.payment.common.request.CreateCustomerRequest;
import com.mydoctorlink.payment.domain.StripeCustomer;
import com.mydoctorlink.payment.stripe.service.CreateCustomerService;
import com.stripe.exception.StripeException;
import com.stripe.model.Customer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CreateCustomerServiceTest {

    @Mock
    StripeCustomer stripeCustomerApi;
    @Mock
    CreateCustomerRequest customerRequest;

    @InjectMocks
    CreateCustomerService createCustomerService;

    @BeforeEach
    void mockSetup() throws Exception {
        doAnswer(invocation->{
            ((CreateCustomerRequest)invocation.getMock()).name = "abc";
            ((CreateCustomerRequest)invocation.getMock()).email = "abc@iid.com";
            return null;
        }).when(customerRequest).selfConstruct("");
    }

    @Test
    void shouldCreateNewCustomer() throws Exception {
        Map<String, Object> customerDetail = new HashMap<>();


        when(stripeCustomerApi.searchByEmail("abc@iid.com")).thenReturn(null);

        customerDetail.put("email","abc@iid.com");
        customerDetail.put("name", "abc");

        when(stripeCustomerApi.create(customerDetail)).thenReturn(new Customer());
        assertThat(createCustomerService.process("")).matches(Pattern.compile("\\{\"status\":\"OK\"(.+)"));
        verify(stripeCustomerApi, times(1)).searchByEmail("abc@iid.com");
        verify(stripeCustomerApi, times(1)).create(customerDetail);
    }

    @Test
    void shouldThrowErrorWhenCustomerFound() throws Exception, StripeException {
        Map<String, Object> customerDetail = new HashMap<>();

        when(stripeCustomerApi.searchByEmail("abc@iid.com")).thenReturn(new Customer());

        customerDetail.put("email","abc@iid.com");
        customerDetail.put("name", "abc");

        assertThat(createCustomerService.process("")).isEqualTo("{\"status\":\"ERROR_OCCURRED\", \"error\":\"Email has been registered\"}");
        verify(stripeCustomerApi, times(1)).searchByEmail("abc@iid.com");
        verify(stripeCustomerApi, never()).create(customerDetail);
    }
}