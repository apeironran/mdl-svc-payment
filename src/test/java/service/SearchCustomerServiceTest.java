package service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mydoctorlink.payment.common.request.SearchCustomerRequest;
import com.mydoctorlink.payment.domain.StripeCustomer;
import com.mydoctorlink.payment.stripe.service.SearchCustomerService;
import com.stripe.model.Customer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.regex.Pattern;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class SearchCustomerServiceTest {

    @Mock
    StripeCustomer stripeCustomer;

    @Mock
    SearchCustomerRequest searchCustomerRequest;

    @InjectMocks
    SearchCustomerService testClass;

    @BeforeEach
    void mockSetup() throws Exception {
        doAnswer(invocation->{
            ((SearchCustomerRequest)invocation.getMock()).email = "test@gic.co";
            return null;
        }).when(searchCustomerRequest).selfConstruct("");
    }

    @Test
    void shouldFindTheCustomer() throws Exception{
        when(stripeCustomer.searchByEmail(any())).thenReturn(new Customer());
        assertThat(testClass.process("")).matches(Pattern.compile("\\{\"status\":\"OK\"(.+)"));
        verify(stripeCustomer).searchByEmail(any());
        verify(searchCustomerRequest).selfConstruct("");
    }



}