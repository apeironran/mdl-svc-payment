package service;

import com.mydoctorlink.payment.common.request.CreatePaymentRequest;
import com.mydoctorlink.payment.domain.StripePaymentIntent;
import com.mydoctorlink.payment.stripe.service.CreatePaymentService;
import com.stripe.exception.StripeException;
import com.stripe.model.PaymentIntent;
import com.stripe.param.PaymentIntentCreateParams;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.regex.Pattern;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CreatePaymentServiceTest {

    @Mock
    CreatePaymentRequest createPaymentRequest;

    @Mock
    StripePaymentIntent stripePaymentApi;

    @InjectMocks
    CreatePaymentService createPaymentService;

    @Test
    void shouldCreatePaymentIntent() throws Exception, StripeException {
        doAnswer(invocation->{
            CreatePaymentRequest request = (CreatePaymentRequest) invocation.getMock();
            request.amount = 1L;
            request.currency = "aud";
            return null;
        }).when(createPaymentRequest).selfConstruct("");

        when(stripePaymentApi.create(
                any(PaymentIntentCreateParams.class)
        )).thenReturn(new PaymentIntent());

        assertThat(createPaymentService.process("")).matches(Pattern.compile("\\{\"status\":\"OK\"(.+)"));
        verify(createPaymentRequest,times(1)).selfConstruct("");
        verify(stripePaymentApi, times(1)).create(any(PaymentIntentCreateParams.class));
    }

    @Test
    void shouldThrowErrorUponStripeError() throws Exception {

        doAnswer(invocation->{
            CreatePaymentRequest request = (CreatePaymentRequest) invocation.getMock();
            request.amount = 1L;
            request.currency = "aud";
            return null;
        }).when(createPaymentRequest).selfConstruct("");

        when(stripePaymentApi.create(any())).thenThrow(new TestException("test","shouldThrowErrorUponStripeError","test",1));
        assertThat(createPaymentService.process("")).matches(Pattern.compile("(.*)\"status\":\"ERROR_OCCURRED\"(.*)"));

    }

    private class TestException extends StripeException{
        protected TestException(String message, String requestId, String code, Integer statusCode) {
            super(message, requestId, code, statusCode);
        }
    }

}