package request;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import com.mydoctorlink.payment.common.request.CreatePaymentRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = CreatePaymentRequest.class)
class CreatePaymentRequestTest {

    @Autowired
    CreatePaymentRequest testClass;

    @Test
    void shouldCreateInstanceFromString() throws Exception {
        String requestString = "{\"amount\": \"1\",\"currency\": \"aud\",\"customerId\": \"123\"}";
        testClass.selfConstruct(requestString);
        assertEquals(1L, testClass.amount);
        assertEquals("aud", testClass.currency);
    }

    @Test
    void shouldCreateJsonStringFromInstance() throws Exception {
        testClass.amount = 1L;
        testClass.currency = "aud";
        testClass.customerId = "123";
        String testResult = testClass.serialization(testClass);
        assertEquals("{\"amount\":1,\"currency\":\"aud\",\"customerId\":\"123\"}", testResult);

    }

    @Test
    void shouldThrowException() {
        assertThrows(JsonProcessingException.class, ()->{
            String requestString = "{\"amount\": \"test\",\"email\": \"kkk@jcie.com\"}";
            testClass.deserialization(requestString);
        });
        assertThrows(MismatchedInputException.class, ()->{
            String requestString = "{\"amount\": \"1\",\"currency\": \"aud\"}";
            testClass.deserialization(requestString);
        });
    }

}