package request;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import com.mydoctorlink.payment.common.request.SearchCustomerRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = SearchCustomerRequest.class)
public class SearchCustomerRequestTest {

    @Autowired
    SearchCustomerRequest testClass;

    @Test
    void shouldCreateInstanceFromString() throws Exception {
        String requestString = "{\"email\": \"kkk@jcie.com\"}";
        testClass.selfConstruct(requestString);
        assertEquals("kkk@jcie.com", testClass.email);
    }

    @Test
    void shouldCreateJsonStringFromInstance() throws Exception {
        testClass.email = "abcd@1234";
        String testResult = testClass.serialization(testClass);
        assertEquals("{\"email\":\"abcd@1234\"}", testResult);

    }

    @Test
    void shouldThrowException() {
        assertThrows(JsonProcessingException.class, ()->{
            String requestString = "{\"abc\": \"test\",\"email\": \"kkk@jcie.com\"}";
            testClass.deserialization(requestString);
        });
        assertThrows(MismatchedInputException.class, ()->{
            String requestString = "{}";
            testClass.deserialization(requestString);
        });
    }

}
