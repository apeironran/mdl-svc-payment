package request;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mydoctorlink.payment.common.request.SearchPaymentRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = SearchPaymentRequest.class)
class SearchPaymentRequestTest {

    @Autowired
    SearchPaymentRequest testClass;

    @Test
    void shouldCreateInstanceFromString() throws Exception {
        String requestString = "{\"paymentIntentId\": \"1\"}";
        testClass.selfConstruct(requestString);
        assertEquals("1", testClass.paymentIntentId);
    }

    @Test
    void shouldCreateJsonStringFromInstance() throws JsonProcessingException {
        testClass.paymentIntentId = "1";
        String testResult = testClass.serialization(testClass);
        assertEquals("{\"paymentIntentId\":\"1\"}", testResult);

    }

    @Test
    void shouldThrowException() {
        assertThrows(JsonProcessingException.class, ()->{
            String requestString = "{\"a\": \"test\"}";
            testClass.deserialization(requestString);
        });
    }

}