package request;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import com.mydoctorlink.payment.common.request.CreateCustomerRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = CreateCustomerRequest.class)
public class CreateCustomerRequestTest {

    @Autowired
    CreateCustomerRequest testClass;

    @Test
    void shouldCreateInstanceFromString() throws JsonProcessingException {
        String requestString = "{\"name\": \"test\",\"email\": \"kkk@jcie.com\"}";
        testClass.selfConstruct(requestString);
        assertEquals("test", testClass.name);
        assertEquals("kkk@jcie.com", testClass.email);
    }

    @Test
    void shouldCreateJsonStringFromInstance() throws JsonProcessingException {
        testClass.email = "abcd@1234";
        testClass.name = "only test";
        String testResult = testClass.serialization(testClass);
        assertEquals("{\"name\":\"only test\",\"email\":\"abcd@1234\"}", testResult);

    }

    @Test
    void shouldThrowException() {
        assertThrows(JsonProcessingException.class, ()->{
            String requestString = "{\"abc\": \"test\",\"email\": \"kkk@jcie.com\"}";
            testClass.deserialization(requestString);
        });

        assertThrows(MismatchedInputException.class, ()->{
            String requestString = "{\"name\": \"test\"}";
            testClass.deserialization(requestString);
        });
    }

}
